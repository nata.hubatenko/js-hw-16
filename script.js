function getFib(number, firstNumber, secondNumber) {
	if (number === 2) {
		return secondNumber;
	} else if (number === 1) {
		return firstNumber;
	} else {
		let f1 = firstNumber, f2 = secondNumber, f3;
		for (let i = 2; i < number; i++) {
			f3 = f1 + f2;
			f1 = f2;
			f2 = f3;
		}
		return f3;
	}
}

function getFibNeg(number, firstNumber, secondNumber) {
	if (number === 0) {
		return alert('Not correct');
	} else {
		let f1 = firstNumber, f2 = secondNumber, f3;
		for (let i = 2; i < number; i++) {
			f3 = f1 - f2;
			f1 = f2;
			f2 = f3;
		}
		return f3;
	}
}

let firstNumber = +prompt('Enter first number');
while ((firstNumber.toFixed() != firstNumber) || isNaN(firstNumber) || firstNumber === "") {
	firstNumber = +prompt('Enter first number, please', '');
}
let secondNumber = +prompt('Enter second number');

while ((secondNumber.toFixed() != secondNumber) || isNaN(secondNumber || secondNumber === "")) {
	secondNumber = +prompt('Enter second number, please', '');
}
let number = +prompt('Enter index number:');
while ((number.toFixed() != number) || isNaN(number) || number === "") {
	number = +prompt('Enter index number, please', '');
}

if (firstNumber > 0 && secondNumber > 0) {
	alert(`Fibonacci number is: ${getFib(number, firstNumber, secondNumber)}`);
} else {
	alert(`Fibonacci number is: ${getFibNeg(number, firstNumber, secondNumber)}`);
}

